const Colors = {
  Red: "rgba(155, 0, 0, 0.5)",
  Gray: "rgba(128, 128, 128, 0.267)",
};

class Hero {
  name = null;
  unitGroups = [];
  color = null;

  constructor(name, color) {
    this.name = name;
    this.color = color;
  }

  getUnitGroups() {
    return this.unitGroups;
  }
}

class UnitsGroupType {
  constructor({ name, damage, health, speed, turnsPerRound, level }) {
    this.name = name;
    this.damage = damage;
    this.health = health;
    this.speed = speed;
    this.turnsPerRound = turnsPerRound;
    this.level = level;
  }
}

const HumansFractionUnitTypes = {
  Pikeman: new UnitsGroupType({
    name: "Pikemans",
    damage: 9,
    health: 10,
    speed: 6,
    turnsPerRound: 1,
    level: 1,
  }),
  Archer: new UnitsGroupType({
    name: "Archers",
    damage: 7,
    health: 21,
    speed: 10,
    turnsPerRound: 2,
    level: 2,
  }),
  Knight: new UnitsGroupType({
    name: "Knights",
    damage: 31,
    health: 82,
    speed: 6,
    turnsPerRound: 1,
    level: 3,
  }),
};

class UnitsGroup {
  constructor({ unitType, unitsCount }) {
    this.unitType = unitType;
    this.unitsCount = unitsCount;
  }
}

function getRandomNumberBetween(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function getRandomUnitGroupOfFraction(fraction, groupMinSize, groupMaxSize) {
  const unitTypeKeys = Object.keys(fraction);
  const randomUnitGroupIndex = getRandomNumberBetween(0, unitTypeKeys.length);
  const unitGroupName = unitTypeKeys[randomUnitGroupIndex];
  const groupSize = getRandomNumberBetween(groupMinSize, groupMaxSize);
  const group = new UnitsGroup({
    unitType: fraction[unitGroupName],
    unitsCount: groupSize,
  });
  console.log(group);
  return group;
}

function getRandomUnitGroupsOfFraction(fraction, from, to, minSize, maxSize) {
  const array = [...new Array(getRandomNumberBetween(from, to))];
  return array.map(() =>
    getRandomUnitGroupOfFraction(fraction, minSize, maxSize)
  );
}

const player1 = new Hero("Gbidin", Colors.Red);

const predefined = [
  new UnitsGroup({
    unitType: HumansFractionUnitTypes.Archer,
    unitsCount: getRandomNumberBetween(10, 40),
  }),
  new UnitsGroup({
    unitType: HumansFractionUnitTypes.Knight,
    unitsCount: getRandomNumberBetween(3, 18),
  }),
  new UnitsGroup({
    unitType: HumansFractionUnitTypes.Pikeman,
    unitsCount: getRandomNumberBetween(15, 90),
  }),
];

[
  ...getRandomUnitGroupsOfFraction(HumansFractionUnitTypes, 2, 4, 6, 35),
  new UnitsGroup({unitType: HumansFractionUnitTypes.Archer, unitsCount: getRandomNumberBetween(1,70)}),
].forEach((group) => player1.unitGroups.push(group));

const NeutralCampUnitTypes = {
  Gnoll: new UnitsGroupType({
    name: "Gnolls",
    damage: 14,
    turnsPerRound: 1,
    speed: 4,
    health: 14,
    level: 1,
  }),
  Troll: new UnitsGroupType({
    name: "Trolls",
    damage: 14,
    turnsPerRound: 1,
    speed: 6,
    health: 21,
    level: 2,
  }),
  Thief: new UnitsGroupType({
    name: "Thiefs",
    damage: 19,
    turnsPerRound: 2,
    speed: 7,
    health: 16,
    level: 3,
  }),
};

const neutralPlayer = new Hero("Neutral", Colors.Gray);

getRandomUnitGroupsOfFraction(
  NeutralCampUnitTypes,
  2,
  6,
  10,
  55
).forEach((group) => neutralPlayer.unitGroups.push(group));

console.log(player1, neutralPlayer);

class Battleground {
  fightQueue = [];
  playerActions = new Map();

  currentPlayer = null;
  currentUnitGroup = null;

  turn = 1;

  groupTurns = 0;

  constructor(player1, player2) {
    this.player1 = player1;
    this.player2 = player2;
    this.initQueue();
    // fastest group !== fastest hero
    this.currentPlayer = this.getFastestPlayer();
    console.log(this);
    this.currentUnitGroup = this.fightQueue[0];
    this.groupTurns = this.currentUnitGroup.unitType.turnsPerRound;
    console.log('Opening player! ', this.currentPlayer);
    if (this.currentPlayer === neutralPlayer) {
      setTimeout(() => {this.playerActions.get(neutralPlayer)();}, 2000);
    }
  }

  initQueue() {
    this.fightQueue = [
      ...this.player1.unitGroups,
      ...this.player2.unitGroups,
    ].sort((unitGroup1, unitGroup2) => {
      return unitGroup2.unitType.speed - unitGroup1.unitType.speed;
    });
  }

  getFastestPlayer() {
    const player1Speed = this.player1.unitGroups.reduce(
      (maxSpeed, nextUnit) =>
        nextUnit.unitType.speed > maxSpeed ? nextUnit.unitType.speed : maxSpeed,
      0
    );
    const player2Speed = this.player2.unitGroups.reduce(
      (maxSpeed, nextUnit) =>
        nextUnit.unitType.speed > maxSpeed ? nextUnit.unitType.speed : maxSpeed,
      0
    );
    if (player1Speed === player2Speed) {
      return this.player1.unitGroups.length < this.player2.unitGroups.length ? this.player1 : this.player2;
    }
    return player1Speed > player2Speed ? this.player1 : this.player2;
  }

  getPlayerOfUnitGroup(unitGroup) {
    if (this.player1.unitGroups.includes(unitGroup)) {
      return this.player1;
    } else {
      return this.player2;
    }
  }

  onPlayerTurn(player, action) {
    this.playerActions.set(player, action);
  }

  getOppositeOfPlayer(player) {
    return player === this.player1 ? this.player2 : this.player1;
  }

  groupAttackGroup(attackingGroup, attackedGroup) {
    console.log(attackedGroup.unitsCount);
    const totalDamage = this.getAttackingGroupDamage(attackingGroup);
    const unitsLoss = this.getUnitsLoss(attackingGroup, attackedGroup);
    this.logDamage(
      totalDamage,
      unitsLoss > attackedGroup.unitsCount
        ? attackedGroup.unitsCount
        : unitsLoss,
      attackedGroup
    );
    attackedGroup.unitsCount = attackedGroup.unitsCount - unitsLoss;
    if (attackedGroup.unitsCount <= 0) {
      const owner = this.getPlayerOfUnitGroup(attackedGroup);
      const indexOfDyingGroup = owner.unitGroups.indexOf(attackedGroup);
      owner.unitGroups.splice(indexOfDyingGroup, 1);
      //   const indexInQueue = this.fightQueue.indexOf(indexOfDyingGroup);
      this.fightQueue = this.fightQueue.filter(
        (group) => group !== attackedGroup
      );
      console.log(this.fightQueue);
    }
    this.groupTurns--;
    console.log(attackedGroup.unitsCount);
  }

  getUnitsLoss(attackingGroup, attackedGroup) {
    const totalLoss = Math.floor(
      this.getAttackingGroupDamage(attackingGroup) /
        attackedGroup.unitType.health
    );
    return totalLoss > attackedGroup.unitsCount
      ? attackedGroup.unitsCount
      : totalLoss;
  }

  getAttackingGroupDamage(attackingGroup) {
    return attackingGroup.unitsCount * attackingGroup.unitType.damage;
  }

  endGroupTurn() {
    const lastGroup = this.fightQueue.shift();
    if (this.fightQueue.length === 0) {
      this.turn++;
      this.initQueue();
      document.querySelector("#turn").innerHTML = this.turn;
      this.log(
        `<div>End of turn ${this.turn - 1}. Turn ${this.turn} starts</div>`
      );
    }
    this.currentUnitGroup = this.fightQueue[0];
    const ownerOfNewGroup = this.getPlayerOfUnitGroup(this.currentUnitGroup);
    this.groupTurns = this.currentUnitGroup.unitType.turnsPerRound;
    if (ownerOfNewGroup === this.player1) {
      this.currentPlayer = this.player1;
    } else {
      this.currentPlayer = this.player2;
    }
    console.log("end of turn", this);
  }

  log(message) {
    document
      .querySelector(".battle-log")
      .insertAdjacentHTML("beforeEnd", message);
    var objDiv = document.querySelector(".battle-log");
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  logDamage(damage, unitsLoss, target) {
    this.log(`
        <div>
            <span class='heavy-text' style='color: ${this.currentPlayer.color}'>
                ${this.currentUnitGroup.unitType.name}
            </span>
            deal ${damage} damage to
            <span class='heavy-text' style='color: ${
              this.getOppositeOfPlayer(this.currentPlayer).color
            }'>
                ${target.unitType.name}
            </span>
            , killing ${unitsLoss} units.
        </div>
        `);
    if (unitsLoss === target.unitsCount) {
      this.log(`
        <div>
            <span class='heavy-text' style='color: ${
              this.getOppositeOfPlayer(this.currentPlayer).color
            }'>
                ${target.unitType.name}
            </span>
            are dying.
        </div>
        `);
    }
  }
}

// todo: group ended turn, new group is taking it's turn

const battleground = new Battleground(player1, neutralPlayer);
battleground.onPlayerTurn(neutralPlayer, function () {
  for (let i = 0; i < battleground.groupTurns; i++) {
    setTimeout(() => {
      const enemyGroups = battleground.getOppositeOfPlayer(neutralPlayer)
        .unitGroups;

      

      const enemyGroupIndex = getRandomNumberBetween(0, enemyGroups.length);
      const randomEnemyGroup = enemyGroups[enemyGroupIndex];

      const untiLoss = battleground.getUnitsLoss(
        battleground.currentUnitGroup,
        randomEnemyGroup
      );
      const groupElem = Array.from(document.querySelectorAll('.left-player .player-unit-group'))[enemyGroupIndex];

      createSfx(
        groupElem,
        `<span class='damage-span'>${-untiLoss}</span>`,
        1500
      );

      battleground.groupAttackGroup(
        battleground.currentUnitGroup,
        randomEnemyGroup
      );
      if (battleground.groupTurns <= 0) {
        battleground.endGroupTurn();
        if (battleground.currentPlayer === neutralPlayer) {
          battleground.playerActions.get(neutralPlayer)();
        }
      }
      rerenderAll();
    }, getRandomNumberBetween(1000, 5000));
  }
});
battleground.onPlayerTurn(player1, function (unitGroup, index, groupElem) {
  console.log("no unit group >> ", unitGroup);

  if (battleground.currentPlayer === player1) {
    const untiLoss = battleground.getUnitsLoss(
      battleground.currentUnitGroup,
      unitGroup
    );
    createSfx(groupElem, `<span class='damage-span'>${-untiLoss}</span>`, 1500);

    battleground.groupAttackGroup(battleground.currentUnitGroup, unitGroup);
    if (battleground.groupTurns <= 0) {
      battleground.endGroupTurn();
      if (battleground.currentPlayer !== player1) {
        console.log("Give turn to NEUTRAL");
        battleground.playerActions.get(neutralPlayer)();
      }
    }
    rerenderAll();
  }
});

/* ideas ? 
  0. unit levels
  1. units can go behind other units, so they cannot be attacked by melee units directly (amount of such moves is limited by tactics points?)
  2. melee units have a damage penalty when attacking ones who are far (speed-binded?). 
  3. a group can be disbanded, so hero will have gold for them (with a penalty, something about 25-33% of their cost)
      this can serve as a tool for exchanging some low level units to a higher tier unit group, and vice versa
  4. heroes with their own abilities and passives (heal/damage missle, armor corruption on attack)
      abilities may be opening with level.
  5. (art style thing) a city can be a large scale place, which develops
  6. damage range (dice system (count of dices + count of edges) or min/max)
*/